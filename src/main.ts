import * as express from 'express';
import * as Chance from 'chance';
import * as Shakespeare from 'shakespeare-data';
import * as zerorpc from 'zerorpc';
import { StringDecoder } from 'string_decoder';
const app = express();
const chance = new Chance();
const decoder = new StringDecoder('utf8');

const zero = new zerorpc.Client();
zero.connect('tcp://127.0.0.1:4242');

app.set('views', './views');
app.set('view engine', 'pug');
app.use(express.static('static'));

app.get('/', async (req, res) => {
	const ran = Math.random();
	let text: string = ' ';
	const amount = chance.integer({ min: 500, max: 20000 });

	let vals = [];

	const titleSyllables = chance.integer({ min: 2, max: 5 });
	const title = chance.prefix() + ' ' + chance.capitalize(chance.word({ syllables: titleSyllables }));

	const lineAmount = chance.integer({ min: 10, max: 20 });
	let linesRaw = [];

	for (let i = 0; i < lineAmount; i++) {
		await getGenLine()
		.catch((e) => {
			console.error(e);
			res.send('internal error');
		})
		.then((r) => {
			linesRaw.push(r);
		});
	}

	let wordArray: string[] = [];

	for (let i = 0; i < linesRaw.length; i++) {
		const lines = (linesRaw[i].split(' '));
		for (let z = 0; z < lines.length; z++) {
			wordArray.push(lines[z]);
		}
		wordArray.push('\n');
	}

	for (let i = 0; i < wordArray.length; i++) {
		vals.push({
			word: wordArray[i] + ' ',
			color: chance.color({ format: 'hex', grayscale: true }),
		});
	}
	res.render('index', { words: vals, title: title });
});

const getGenLine = function (): Promise<string> {
	return new Promise((resolve, reject) => {
		zero.invoke('getLine', (e, r, more) => {
			if (e) {
				reject(e);
			} else {
				resolve(decoder.write(r));
			}
		});
	});
};

app.listen(8080, () => console.log('listening...'));
