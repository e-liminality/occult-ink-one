import markovify
import nltk
import re
import zerorpc


# Get raw text as string.
with open("./corpus.txt") as f:
    text = f.read()

class POSifiedText(markovify.Text):
    def word_split(self, sentence):
        words = re.split(self.word_split_pattern, sentence)
        words = [ "::".join(tag) for tag in nltk.pos_tag(words) ]
        return words

    def word_join(self, words):
        sentence = " ".join(word.split("::")[0] for word in words)
        return sentence

# Build the model.
text_model = POSifiedText(text)

class HelloRPC(object):
    def getLine(self):
        return "%s" % text_model.make_sentence(tries=100)


print('ready')

s = zerorpc.Server(HelloRPC())
s.bind("tcp://0.0.0.0:4242")
s.run()
